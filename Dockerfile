FROM mhart/alpine-node:12 AS builder
RUN apk --no-cache --virtual build-dependencies add \
    python \
    make \
    g++

WORKDIR /term/app
COPY app .
RUN npm ci --prod && npm run build && apk del build-dependencies

WORKDIR /term/server
COPY server .
RUN npm ci --prod


FROM mhart/alpine-node:slim-12
WORKDIR /term/app
COPY --from=builder /term/app/build .
WORKDIR /term/server
COPY --from=builder /term/server .

RUN addgroup -S node && adduser -S node -G node
RUN addgroup ninja && adduser lolcat -D -G ninja && echo "lolcat:cowsay" | chpasswd
RUN echo "root:greetz$(date +%s)" | chpasswd
RUN apk add --no-cache openssh bind-tools whois && ssh-keygen -A
RUN chown -R node:node /term

RUN chmod -R o= /term
RUN rm /etc/*-release

COPY motd /etc

EXPOSE 80

COPY entrypoint.sh /
RUN chmod 100 /entrypoint.sh
ENTRYPOINT /entrypoint.sh
