import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';

import 'xterm/css/xterm.css';

ReactDOM.render(<App />, document.getElementById('root'));
