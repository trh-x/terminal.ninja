# Terminal Ninja

A mashup of [`Three.JS`](https://threejs.org/) and an [`SSH terminal`](https://en.wikipedia.org/wiki/Secure_Shell), using [`react-three-fiber`](https://github.com/react-spring/react-three-fiber) and [`xterm.js`](https://github.com/xtermjs/xterm.js/) / [`webssh2`](https://github.com/billchurch/webssh2).

Online at http://terminal.ninja :).
