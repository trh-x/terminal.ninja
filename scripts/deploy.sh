#!/bin/bash
set -e

# Default values
REGISTRY_PORT="5000"
COMMAND=""
DEBUG=false
PORT="8000"

# Help message
show_help() {
    cat <<EOT
Usage: $0 [options] command [args]

Commands:
  build              Build the terminal.ninja application
  up                 Start the service
  down               Stop the service
  setup-remote       Configure remote deployment
  secure-remote      Install and configure security rules on remote host
  deploy             Deploy to remote server
  test-registry      Test connection to registry through SSH tunnel

Options:
  -h, --help         Show this help message
  -d, --debug        Enable debug/verbose output
  -p, --port <port>  Specify port (default: 80)
EOT
}

# Debug logging function
debug_log() {
    if [ "$DEBUG" = true ]; then
        echo "[DEBUG] $1"
    fi
}

# Parse command line arguments
while [[ "$#" -gt 0 ]]; do
    case $1 in
        -h|--help) 
            show_help
            exit 0 
            ;;
        -d|--debug) 
            DEBUG=true
            ;;
        -p|--port)
            shift
            PORT="$1"
            ;;
        build|up|down|setup-remote|deploy|test-registry|secure-remote)
            COMMAND="$1"
            ;;
        *)
            echo "Unknown parameter: $1"
            exit 1
            ;;
    esac
    shift
done

# Check if a command was specified
if [ -z "$COMMAND" ]; then
    echo "No command specified"
    show_help
    exit 1
fi

# Function to cleanup SSH tunnel
cleanup_tunnel() {
    if [ -n "$TUNNEL_PID" ] && ps -p $TUNNEL_PID > /dev/null; then
        echo "Cleaning up SSH tunnel..."
        kill $TUNNEL_PID
    fi
}

# Function to setup SSH tunnel
setup_tunnel() {
    local remote_host="$1"
    echo "Setting up SSH tunnel for registry access..."
    
    # Check for existing process on the port
    if ss -ln | grep -q ":${REGISTRY_PORT}\\b"; then
        echo "Error: Port ${REGISTRY_PORT} is already in use"
        exit 1
    fi
    
    # Start new tunnel
    ssh -f -N -L "${REGISTRY_PORT}:localhost:${REGISTRY_PORT}" "$remote_host"
    TUNNEL_PID=$(ss -lpn | grep ":${REGISTRY_PORT}" | grep -o 'pid=\([0-9]*\)' | cut -d= -f2)
    
    # Set up cleanup trap
    trap cleanup_tunnel EXIT
    
    # Wait for tunnel to be ready
    for i in {1..5}; do
        if nc -z localhost ${REGISTRY_PORT} 2>/dev/null; then
            return 0
        fi
        sleep 1
    done
    
    echo "Timeout waiting for tunnel to be ready"
    cleanup_tunnel
    exit 1
}

# Function to setup remote context
setup_remote() {
    echo "Setting up remote Docker context..."
    read -p "Enter remote server host: " host
    read -p "Enter SSH user: " user
    read -p "Enter SSH port (default: 22): " port
    port=${port:-22}
    
    # Create context
    docker context create terminal-ninja-prod \
        --docker "host=ssh://$user@$host:$port" \
        --description "Production server at $host"
    
    echo "Testing connection..."
    if docker --context terminal-ninja-prod info >/dev/null 2>&1; then
        echo "Connection successful!"
        
        read -p "Would you like to create a registry? [y/N] " create_registry
        if [[ "$create_registry" =~ ^[Yy]$ ]]; then
            docker --context terminal-ninja-prod run -d \
                --restart=always \
                --name registry \
                --network host \
                -v registry_data:/var/lib/registry \
                -e REGISTRY_HTTP_ADDR=localhost:${REGISTRY_PORT} \
                registry:2
            
            echo "Registry created on ${host}:${REGISTRY_PORT}"
        fi
    else
        echo "Connection failed. Please check your SSH configuration."
        docker context rm terminal-ninja-prod
        exit 1
    fi
}

# Function to deploy
deploy() {
    echo "Deploying to remote server..."
    
    if ! docker --context terminal-ninja-prod info >/dev/null 2>&1; then
        echo "Cannot connect to remote server. Please run setup-remote first."
        exit 1
    fi

    # Get remote host from context
    REMOTE_HOST=$(docker context inspect terminal-ninja-prod --format '{{.Endpoints.docker.Host}}' | sed 's|ssh://||' | cut -d':' -f1)
    
    # Setup tunnel
    setup_tunnel "$REMOTE_HOST"
    REGISTRY="localhost:${REGISTRY_PORT}"

    # Build locally and push to registry
    docker context use default
    
    echo "Building image..."
    docker compose build

    # Tag and push
    echo "Pushing image to registry..."
    docker tag terminal-ninja:latest "${REGISTRY}/terminal-ninja:latest"
    docker push "${REGISTRY}/terminal-ninja:latest"

    # Deploy to remote
    echo "Deploying to remote host..."
    docker context use terminal-ninja-prod
    
    # Set registry prefix and deploy
    export REGISTRY_PREFIX="localhost:${REGISTRY_PORT}/"
    export PORT="$PORT"
    
    # Just pull and run, no building on remote
    docker compose pull
    docker compose up -d
    
    # Switch back to default context
    docker context use default
    echo "Deployment complete!"
}

# Main command execution
case $COMMAND in
    "build")
        docker compose build
        ;;
    "up")
        docker compose up -d
        ;;
    "down")
        docker compose down
        ;;
    "setup-remote")
        setup_remote
        ;;
    "deploy")
        deploy
        ;;
    "test-registry")
        if ! docker --context terminal-ninja-prod info >/dev/null 2>&1; then
            echo "Cannot connect to remote server. Please run setup-remote first."
            exit 1
        fi
        REMOTE_HOST=$(docker context inspect terminal-ninja-prod --format '{{.Endpoints.docker.Host}}' | sed 's|ssh://||' | cut -d':' -f1)
        setup_tunnel "$REMOTE_HOST"
        curl -v http://localhost:${REGISTRY_PORT}/v2/
        ;;
    "secure-remote")
        if ! docker --context terminal-ninja-prod info >/dev/null 2>&1; then
            echo "Cannot connect to remote server. Please run setup-remote first."
            exit 1
        fi

        # Extract SSH details from Docker context
        REMOTE_SSH=$(docker context inspect terminal-ninja-prod --format '{{.Endpoints.docker.Host}}' | sed 's|ssh://||')
        SSH_USER=$(echo "$REMOTE_SSH" | cut -d'@' -f1)
        SSH_HOST=$(echo "$REMOTE_SSH" | cut -d'@' -f2 | cut -d':' -f1)
        SSH_PORT=$(echo "$REMOTE_SSH" | cut -d':' -f2)

        log "Running security hardening on remote host..."
        ssh -l "$SSH_USER" -p "$SSH_PORT" "$SSH_HOST" "sudo -S bash -s" < scripts/secure-host.sh
        ;;
    "")
        echo "No command specified"
        show_help
        exit 1
        ;;
esac 