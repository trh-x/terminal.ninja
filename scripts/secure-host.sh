#!/bin/bash

set -euo pipefail

# Colors for output
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

log() {
    echo -e "${GREEN}[+]${NC} $1"
}

error() {
    echo -e "${RED}[!]${NC} $1"
}

get_docker_subnet() {
    if ! command -v docker &> /dev/null; then
        error "Docker is not installed"
        exit 1
    }

    # Get the bridge network subnet using docker network inspect
    local subnet
    subnet=$(docker network inspect bridge --format '{{range .IPAM.Config}}{{.Subnet}}{{end}}')

    if [ -z "$subnet" ]; then
        error "Failed to determine Docker bridge network subnet"
        exit 1
    }

    echo "$subnet"
}

# Check if running as root
if [ "$EUID" -ne 0 ]; then
    error "Please run as root"
    exit 1
fi

# Check if conntrack is installed
if ! command -v conntrack &> /dev/null; then
    log "Installing conntrack..."
    apt-get update
    apt-get install -y conntrack
else
    log "conntrack is already installed"
fi

# Get Docker subnet
DOCKER_SUBNET=$(get_docker_subnet)
log "Detected Docker bridge network subnet: $DOCKER_SUBNET"

# Check if iptables rule exists
if ! iptables -C DOCKER-USER --src "$DOCKER_SUBNET" -m conntrack --ctstate NEW -j DROP 2>/dev/null; then
    log "Adding iptables rule to block new connections from Docker subnet..."
    iptables -I DOCKER-USER --src "$DOCKER_SUBNET" -m conntrack --ctstate NEW -j DROP
else
    log "iptables rule already exists"
fi

log "Security hardening complete!" 