const express = require('express');
const path = require('path');
const { setupServer } = require('@techanvil/webssh2/server/app')

const app = express();

const staticPath = process.argv[2] || '../app/build';

app.use(express.static(path.join(__dirname, staticPath)));

const { server, config } = setupServer(app);

server.listen({ host: config.listen.ip, port: config.listen.port })
